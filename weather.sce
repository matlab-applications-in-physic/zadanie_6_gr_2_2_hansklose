/*
Oskar Klos, Technical Physics
following program is prepared for three already assumed stations
program requires connection to the internet in order to work properly
another requirement is Json Toolbox, following line installs it, if missing please restart the program
*/


atomsInstall('json');

//following function calculates the wind chill; source - pl.wikipedia.org/wiki/Temperatura_odczuwalna Access 13.12.19 11:25
function Tf = Windchill(temp,w_vel)
    Tf = 13.12+0.6215* temp - 11.37 * w_vel**(0.16) + 0.3965 * temp * w_vel
end

//following function calculates 

//required sets of headers

headers = ['miasto','data pomiaru','godzina pomiaru','temperaura rzeczywista','temperatura odczuwalna', 'cisnienie', 'predkosc wiatru','wilgotnosc'];
headers_windchill = ['miasto','data pomiaru','godzina pomiaru','temperatura odczuwalna'];
headers_maxmin = ['miasto','data pomiaru','godzina pomiaru','temperaura maksymalna','temperatura minimalna'];


//beginning of a while loop which will run the rest of the program every hour the rest is written is lines x and x
while %T        

//obtaining the data for all cities from two different sources  
//Raciborz
URL1(1) = JSONParse(mgetl(getURL('https://danepubliczne.imgw.pl/api/data/synop/id/12540')));
URL2(1)=JSONParse(mgetl(getURL ("http://wttr.in/raciborz?format=j1")));
//Katowice
URL1(2) = JSONParse(mgetl(getURL('https://danepubliczne.imgw.pl/api/data/synop/id/12560')));
URL2(2)=JSONParse(mgetl(getURL ("https://wttr.in/katowice?format=j1")));
//Bielsko Biala
URL1(3) = JSONParse(mgetl(getURL('https://danepubliczne.imgw.pl/api/data/synop/id/12600')));
URL2(3)=JSONParse(mgetl(getURL ("https://wttr.in/czestochowa?format=j1")));

//following lines:
//calculate the Windchill and convert to string using IMGW data within the loop which is then immediately used for the lists
//saves up the data in order as in the "headers" list from both sources

for i = 1:3
   w(i)=string(Windchill (strtod(URL1(i).temperatura),strtod(URL1(i).predkosc_wiatru))) ;
   imgw(i) = [URL1(i).stacja,URL1(i).data_pomiaru,URL1(i).godzina_pomiaru,URL1(i).temperatura,w(i),URL1(i).cisnienie,URL1(i).predkosc_wiatru,URL1(i).wilgotnosc_wzgledna];
   t_odczuwalna(i) = [URL1(i).stacja,URL1(i).data_pomiaru,URL1(i).godzina_pomiaru,w(i)];
   wttr(i) = [URL1(i).stacja,URL2(i).weather.date,URL2(i).current_condition.observation_time,URL2(i).current_condition.temp_C ,URL2(i).current_condition.FeelsLikeC,URL2(i).current_condition.pressure,URL2(i).current_condition.windspeedKmph,URL2(i).current_condition.humidity];
end

file_name=['imgw_weather_raciborz','imgw_weather_katowice','imgw_weather_czestochowa','wttr_weather_raciborz','wttr_weather_katowice','wttr_weather_czestochowa']

Weather = mopen('weatherData.csv','a+');

for i = 1:3
    csvWrite(imgw(i),weatherData.csv,",");
    csvWrite(wttr(i),weatherData.csvm",");
end
    
mclose('all');


sleep(3600,"s")
end

